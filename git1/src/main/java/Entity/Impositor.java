/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table; 

/**
 *
 * @author Administrador
 */
@Entity
@Table
public class Impositor  implements Serializable{
    //variables
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @JoinColumn(referencedColumnName = "cliente")
    @ManyToOne
    private Cliente cliente;
    @JoinColumn(referencedColumnName = "cuenta")
    @ManyToOne
    private Cuenta cuenta;
    //Metodos get y set para todo

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }
    //funcion equl y hasCode solo id

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Impositor other = (Impositor) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    //metodo toString solo id

    @Override
    public String toString() {
        return "Impositor{" + "id=" + id + '}';
    }
    //constructor
    public Impositor() {
    }
    
}
